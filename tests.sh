#!/bin/bash
#
# Author: Bertrand Benoit <mailto:contact@bertrand-benoit.net>
# Description: Tests all features provided by utilities script.
# Version: 1.1
#
# Optional variables you can define before starting the tests:
#  BSC_TESTS_RUNASROOT              0|1   indicate tests are launched with root user (like in CI/CD docker image)

#BSC_DEBUG_UTILITIES=1
export BSC_CATEGORY="tests:general"
export BSC_DISABLE_ERROR_TRAP=1
export BSC_BSC_ERROR_MESSAGE_EXITS_SCRIPT=0
export BSC_LOG_CONSOLE_OFF=1

currentDir=$( dirname "$( command -v "$0" )" )
shunitUtilities="$currentDir/shunit2/shunit2"
[ ! -f "$shunitUtilities" ] && echo -e "ERROR: shunit2 utilities not found, you must initialize your git submodule once after you cloned the repository (or use the --recursive option while cloning):\ngit submodule init\ngit submodule update" >&2 && exit 1

# Ensures utilities path has been defined, and sources it.
[ -z "${SCRIPTS_COMMON_PATH:-}" ] && echo "SCRIPTS_COMMON_PATH environment variable must be defined." >&2 && exit 1
[ ! -f "$SCRIPTS_COMMON_PATH" ] && echo -e "ERROR: scripts-common utilities not found, you must install it before using this script or adapt your SCRIPTS_COMMON_PATH environment variable (checked path: $SCRIPTS_COMMON_PATH)" >&2 && exit 1
# shellcheck disable=1090
. "$SCRIPTS_COMMON_PATH"

## Defines some constants.
## Now uses the scripts-common v2.1+ Daemon samples.
declare -r miscDir="$( dirname "$SCRIPTS_COMMON_PATH" )/misc"
declare -r daemonSample1="$miscDir/daemonScriptLaunchingThirdPartyTool.sh.sample"
declare -r daemonSample2="$miscDir/daemonScriptLaunchingItsOwnInstructions.sh.sample"

# usage: enteringTests <test category>
function enteringTests() {
  local _testCategory="$1"
  BSC_CATEGORY="tests:$_testCategory"

  info "$_testCategory feature tests - BEGIN"
}

# usage: exitingTests <test category>
function exitingTests() {
  local _testCategory="$1"
  BSC_CATEGORY="tests:general"

  info "$_testCategory feature tests - END"
}

# Usage: _createSimpleConfigurationFile <config file> <single config key> <config value>
_createSimpleConfigurationFile() {
    local _configFile="$1" _configKey="$2" _configValue="$3"

    export BSC_CONFIG_FILE="$_configFile"
cat > "$BSC_CONFIG_FILE" <<EOF
  $_configKey="$_configValue"
EOF
}

## Define Tests functions.
# Logger feature Tests.
testLoggerFeature() {
  enteringTests "logger"

  # Tests info function with/out verbose mode.
  BSC_VERBOSE=0 info "Simple message tests (should not have prefix)" || fail "Logger INFO level"
  BSC_VERBOSE=1 info "Simple message tests (should not have prefix)" || fail "Logger INFO level"

  # Tests various message functions.
  writeMessage "Info message test" || fail "Logger NORMAL level"
  warning "Warning message test" || fail "Logger WARNING level"

  # Tests error message function with some various configuration.
  errorMessage "Error message test" -1  || fail "Logger ERROR level" # -1 to avoid automatic exit of the script
  BSC_ERROR_MESSAGE_EXITS_SCRIPT=0 errorMessage "Error message test" && fail "Logger ERROR level" # Alternatively BSC_ERROR_MESSAGE_EXITS_SCRIPT can be used

  # Tests message function with BSC_LOG_CONSOLE_OFF=0 (by default during tests it is 1).
  BSC_LOG_CONSOLE_OFF=0 writeMessage "Info message test" >>"$BSC_LOG_FILE" || fail "Logger INFO level"

  writeOK || fail "writeOK special function"
  BSC_LOG_CONSOLE_OFF=0 writeOK  >>"$BSC_LOG_FILE" || fail "writeOK special function"

  writeNotFound || fail "writeNotFound special function"
  BSC_LOG_CONSOLE_OFF=0 writeNotFound  >>"$BSC_LOG_FILE" || fail "writeNotFound special function"

  exitingTests "logger"
}

# Robustness Tests.
testLoggerRobustness() {
  local _logLevel _message _newLine _exitStatus

  enteringTests "robustness"

  _logLevel="$_BSC_LOG_LEVEL_MESSAGE"
  _message="Simple message"
  _newLine="1"
  _exitStatus="-1"

  # doWriteMessage should NOT be called directly, but user can still do it, ensures robustness on parameters control.
  # Log level
 _doWriteMessage "Broken Log level ..." "$_message" "$_newLine" "$_exitStatus" || fail "Broken Log level not detected"

  # Message
 _doWriteMessage "$_logLevel" "Message on \
                                several \
                                lines" "$_newLine" "$_exitStatus" || fail "Logger message on several lines, badly managed"

 _doWriteMessage "$_logLevel" "Message on \nseveral \nlines" "$_newLine" "$_exitStatus" || fail "Logger message on several lines, badly managed"
  # New line.
 _doWriteMessage "$_logLevel" "$_message" "Bad value" "$_exitStatus" || fail "Logger Bad value (new line), badly managed"

  # Exit status.
 _doWriteMessage "$_logLevel" "$_message" "$_newLine" "Bad value" || fail "Logger Bad value (exit status), badly managed"

  exitingTests "robustness"
}

# Tests DumpCall function.
_innerLevel2Func() {
  # Triggers the dumpFuncCall
  # Can't do something like the following line because shunit2 stops on it:
  #[ 1 -eq 0 ]
  dumpFuncCall "$BSC_ERROR_DEFAULT"
}

_innerLevel1Func() {
  _innerLevel2Func
}

testDumpCallFunction() {
  enteringTests "dumCall function"

  # N.B.: should never be called directly, but it is done here for tests purposes only.
  _innerLevel1Func || fail "dumpFuncCall should have been triggered, managed, and return success to complete the script"

  exitingTests "dumCall function"
}

# Environment check feature Tests.
testEnvironmentCheckFeature() {
  enteringTests "envCheck"

  if [ "${BSC_TESTS_RUNASROOT:-0}" -eq 0 ]; then
    assertFalse "Checking if user is root" isRootUser
  else
    assertTrue "Checking if user is root" isRootUser
  fi

  LANG=en_GB checkLocale && fail "Checking Locale with no utf-8 LANG"
  LANG=zz_ZZ.UTF-8 checkLocale && fail "Checking Locale with not installed/existing utf-8 LANG"
  LANG=en_GB.UTF-8 checkLocale || fail "Checking Locale with a good LANG defined to en_GB.UTF-8"

  exitingTests "envCheck"
}

# Conditional Tests.
testConditionalBehaviour() {
  enteringTests "conditional"

  # Script should NOT break because of the pipe status ...
  # shellcheck disable=2050
  [ 0 -gt 1 ] || info "fake test ..."

  exitingTests "conditional"
}

# Version feature Tests.
testVersionFeature() {
  local _scriptsCommonDir _fileWithVersion _version _fakeVersion _fakeVersion2 _fakeVersion3
  enteringTests "version"

  _scriptsCommonDir=$( dirname "$SCRIPTS_COMMON_PATH" )
  _fileWithVersion="$_scriptsCommonDir/README.md"
  _version=$( getVersion "$_fileWithVersion" )
  _fakeVersion="999.999.999"
  _fakeVersion2="999.999.999.1"
  _fakeVersion3="999.999.998"

  writeMessage "scripts-common version: $_version"
  writeMessage "scripts-common detailed version: $( getDetailedVersion "$_version" "$_scriptsCommonDir" )"

  # Limit cases.
  getVersion "notExistingFile" >>"$BSC_LOG_FILE" || fail "getVersion on NOT existing file should not failed"
  assertEquals "$( getVersion "notExistingFile" "$_fakeVersion" )" "$_fakeVersion" || fail "getVersion should return specified default version"

  getDetailedVersion "$_version" "$currentDir/NotExistingDirectory" && fail "Checking getDetailedVersion on NOT existing directory"

  # Tests with version with first number different.
  isVersionGreater "$_version" "$_fakeVersion" && fail "Checking if $_version is greater than $_fakeVersion"
  ! isVersionGreater "$_fakeVersion" "$_version" && fail "Checking if $_fakeVersion is greater than $_version"

  # Tests with version with first number equals, and various further parts.
  isVersionGreater "$_fakeVersion" "$_fakeVersion2" && fail "Checking if $_fakeVersion is greater than $_fakeVersion2"
  ! isVersionGreater "$_fakeVersion2" "$_fakeVersion" && fail "Checking if $_fakeVersion2 is greater than $_fakeVersion"

  isVersionGreater "$_fakeVersion3" "$_fakeVersion" && fail "Checking if $_fakeVersion3 is greater than $_fakeVersion"
  ! isVersionGreater "$_fakeVersion" "$_fakeVersion3" && fail "Checking if $_fakeVersion is greater than $_fakeVersion3"

  # By default, same version are not regarded are greater, but OK with specific option.
  isVersionGreater "$_version" "$_version" && fail "Checking if $_version is greater than itself should failed by default"
  isVersionGreater "$_version" "$_version" 1 || fail "Checking if $_version is greater than itself should succeed with option"

  exitingTests "version"
}

# Time feature Tests.
testTimeFeature() {
  enteringTests "time"

  initializeStartTime || fail "Time feature initialization"
  sleep 1
  info "Uptime: $( getUptime )" || fail "Time feature read"

  finalizeStartTime || fail "Time feature finalization"

  exitingTests "time"
}

testCheckPathFeature() {
  local _checkPathRootDir="$BSC_TMP_DIR/checkPathRootDir"
  local _dataFileName="myFile.data"
  local _binFileName="myFile.bin"
  local _subPathDir="myDirectory"
  local _homeRelativePath="something/not/existing"
  local _pathsToFormatBefore _pathsToFormatAfter

  enteringTests "checkPath"

  # To avoid error when configuration key is not found, switch on this mode.
  BSC_MODE_CHECK_CONFIG=1

  # Limit tests, on not existing files.
  isEmptyDirectory "$_checkPathRootDir" && fail "Checking NOT existing directory, is empty (should answer NO)"

  updateStructure "$_checkPathRootDir" || fail "Update directories structure"

  checkDataFile "$_checkPathRootDir/$_dataFileName" && fail "Checking NOT existing Data file"

  checkPath "$_checkPathRootDir/$_subPathDir" && fail "Checking NOT existing Path"

  checkBin "$_checkPathRootDir/$_binFileName" && fail "Checking NOT existing Binary file"

  # Normal situation.
  touch "$_checkPathRootDir/$_dataFileName" "$_checkPathRootDir/$_binFileName" || fail "File creation"
  updateStructure "$_checkPathRootDir/$_subPathDir" || fail "Update directories structure 2"

  isEmptyDirectory "$_checkPathRootDir/$_subPathDir" || fail "Checking existing directory is empty"

  checkDataFile "$_checkPathRootDir/$_dataFileName" || fail "Checking existing Data file"

  checkPath "$_checkPathRootDir/$_subPathDir" || fail "Checking existing Path"

  # checkBin should check existence + 'execute' permission.
  checkBin "$_checkPathRootDir/$_binFileName" && fail "Checking existing Binary file should fail if 'execute' permission is missing"
  chmod +x "$_checkPathRootDir/$_binFileName" || fail "File permission update"
  checkBin "$_checkPathRootDir/$_binFileName" || fail "Checking existing Binary file with 'execute' permission should succeed"

  # Absolute/Relative path and completePath.
  isAbsolutePath "$_checkPathRootDir" || fail "Checking isAbsolutePath function on absolute path"
  isAbsolutePath "$_subPathDir" && fail "Checking isAbsolutePath function on relative path"

  isRelativePath "$_checkPathRootDir" && fail "Checking isRelativePath function on absolute path"
  isRelativePath "$_subPathDir" || fail "Checking isRelativePath function on relative path"

  # Absolute path stays unchanged.
  assertEquals "$( buildCompletePath "$_checkPathRootDir" 0 )" "$_checkPathRootDir" || fail "Checking buildCompletePath function on absolute path"

  # Relative path stays unchanged, if prepend disabled.
  assertEquals "$( buildCompletePath "$_subPathDir" 0 )" "$_subPathDir" || fail "Checking buildCompletePath function on relative path, should stay unchanged"
  assertEquals "$( buildCompletePath "$_subPathDir" 0 "$_checkPathRootDir" )" "$_subPathDir" || fail "Checking buildCompletePath function on relative path, should stay unchanged with not prepend option"

  # Relative path must be fully completed, with all prepend arguments.
  assertEquals "$( buildCompletePath "$_subPathDir" 1 "$_checkPathRootDir" )" "$_checkPathRootDir/$_subPathDir" || fail "Checking buildCompletePath function on relative path with prepend option"

  # Special situation: HOME subsitution.
  # "Tilde does not expand in quotes" => it is EXACTLY what we want here because it is the role of buildCompletePath function.
  # shellcheck disable=2088
  assertEquals "$( buildCompletePath "~/$_homeRelativePath" )" "$HOME/$_homeRelativePath" || fail "Checking buildCompletePath function, for ~ substitution with HOME environment variable"

  # checkAndFormatPath Tests.
  # N.B.: at end, use a wildcard instead of the ending 'ry' part.
  _pathsToFormatBefore="$_subPathDir:~/$_homeRelativePath:${_subPathDir/ry/}*"
  _pathsToFormatAfter="$_checkPathRootDir/$_subPathDir:$HOME/$_homeRelativePath:$_checkPathRootDir/$_subPathDir"
  assertEquals "$( checkAndFormatPath "$_pathsToFormatBefore" "$_checkPathRootDir" )" "$_pathsToFormatAfter" || fail "Checking checkAndFormatPath function"

  # Same test on checkAndFormatPath with BSC_MODE_CHECK_CONFIG off.
  BSC_MODE_CHECK_CONFIG=0 assertEquals "$( checkAndFormatPath "$_pathsToFormatBefore" "$_checkPathRootDir" )" "$_pathsToFormatAfter" || fail "Checking checkAndFormatPath function"


  # Very important to switch off this mode to keep on testing others features.
  export BSC_MODE_CHECK_CONFIG=0

  exitingTests "checkPath"
}

# Configuration file feature Tests.
testConfigurationFileFeature() {
  local _configKey="my.config.key"
  local _configValue="my Value"
  local _configFileSimple="$BSC_TMP_DIR/localConfigurationFile.conf"

  local _configDirPath="$BSC_TMP_DIR/testConfigurationFileFeature"
  local _configDataFileName="myTestData" _configPathSimple="myTestDir" _configPathComplex="my/Test/Dir"
  local _configBinFileName="myExecutable.sh"

  enteringTests "config"

  # To avoid error when configuration key is not found, switch on this mode.
  export BSC_MODE_CHECK_CONFIG=1

  ### Tests on $BSC_CONFIG_TYPE_OPTION feature.

  # No configuration file defined, it should not be found.
  info "A configuration key '$BSC_CONFIG_NOT_FOUND' should happen."
  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_OPTION"
  assertEquals "$BSC_LAST_READ_CONFIG" "$BSC_CONFIG_NOT_FOUND" || fail "Configuration not found, badly detected"

  # Create a simple configuration file (including comment, and extra spaces which should be ignored).
  info "Creating the temporary simple configuration file '$_configFileSimple', and configuration key should then be found."
  export BSC_CONFIG_FILE="$_configFileSimple"
cat > "$BSC_CONFIG_FILE" <<EOF
#$_configKey="WRONG$_configValue"
  $_configKey="$_configValue"
#$_configKey="WRONG$_configValue"
EOF

  # Tests checkAndSetConfig.
  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_OPTION"
  assertEquals "$_configValue" "$BSC_LAST_READ_CONFIG" || fail "Configuration should have been found, and have good value"

  # The same should work with BSC_MODE_CHECK_CONFIG off.
  BSC_MODE_CHECK_CONFIG=0 checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_OPTION"
  assertEquals "$_configValue" "$BSC_LAST_READ_CONFIG" || fail "Configuration should have been found, and have good value"

  # Environment creation.
  updateStructure "$_configDirPath" || fail "Update structure"

  ### Tests on $BSC_CONFIG_TYPE_DATA feature.
  # Creates the Data file.
  touch "$_configDirPath/$_configDataFileName"

  _createSimpleConfigurationFile "$BSC_CONFIG_FILE" "$_configKey" "$_configDataFileName"
  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_DATA"
  assertEquals "$BSC_CONFIG_NOT_FOUND" "$BSC_LAST_READ_CONFIG" || fail "Data file configuration should NOT be validated without good path"

  BSC_ROOT_DIR="$_configDirPath" checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_DATA"
  assertEquals "$_configDirPath/$_configDataFileName" "$BSC_LAST_READ_CONFIG" || fail "Data file configuration should be validated with overriden BSC_ROOT_DIR"

  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_DATA" "$_configDirPath"
  assertEquals "$_configDirPath/$_configDataFileName" "$BSC_LAST_READ_CONFIG" || fail "Data file configuration should be validated with specified path to searh into"

  ### Tests on $BSC_CONFIG_TYPE_PATH feature.
  # Tests with a simple relative path.
  _createSimpleConfigurationFile "$BSC_CONFIG_FILE" "$_configKey" "$_configPathSimple"
  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_PATH"
  assertEquals "$BSC_CONFIG_NOT_FOUND" "$BSC_LAST_READ_CONFIG" || fail "relative Path configuration should NOT be validated without good parent path"

  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_PATH"
  assertEquals "$BSC_CONFIG_NOT_FOUND" "$BSC_LAST_READ_CONFIG" || fail "relative Path configuration should failed if it does not exists (with default option)"

  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_PATH" "$_configDirPath" 0
  assertEquals "$_configDirPath/$_configPathSimple" "$BSC_LAST_READ_CONFIG" || fail "relative Path configuration should be OK when accepting it does not exist yet"

  # Creates the Path.
  updateStructure "$_configDirPath/$_configPathSimple" || fail "Update structure"
  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_PATH" "$_configDirPath"
  assertEquals "$_configDirPath/$_configPathSimple" "$BSC_LAST_READ_CONFIG" || fail "relative Path configuration should be OK when parent path is configured properly"

  # Tests with a more complex relative path.
  _createSimpleConfigurationFile "$BSC_CONFIG_FILE" "$_configKey" "$_configPathComplex"
  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_PATH" "$_configDirPath"
  assertEquals "$BSC_CONFIG_NOT_FOUND" "$BSC_LAST_READ_CONFIG" || fail "relative Path configuration should failed if it does not exists (with default option)"

  # Creates a little more complex Path.
  updateStructure "$_configDirPath/$_configPathComplex" || fail "Update structure"
  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_PATH" "$_configDirPath"
  assertEquals "$_configDirPath/$_configPathComplex" "$BSC_LAST_READ_CONFIG" || fail "relative Path configuration should be OK when parent path is configured properly"

  # Tests with an absolute path.
  local _absolutePath="$_configDirPath/2-$_configPathSimple/$_configPathComplex"
  _createSimpleConfigurationFile "$BSC_CONFIG_FILE" "$_configKey" "$_absolutePath"
  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_PATH"
  assertEquals "$BSC_CONFIG_NOT_FOUND" "$BSC_LAST_READ_CONFIG" || fail "absolute Path configuration should failed if it does not exists (with default option)"

  # Creates the absolute Path.
  updateStructure "$_absolutePath" || fail "Update structure"
  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_PATH"
  assertEquals "$_absolutePath" "$BSC_LAST_READ_CONFIG" || fail "absolute Path configuration should be OK when it exists"

  ### Tests on $BSC_CONFIG_TYPE_BIN feature.
  _createSimpleConfigurationFile "$BSC_CONFIG_FILE" "$_configKey" "$_configBinFileName"
  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_BIN"
  assertEquals "$BSC_CONFIG_NOT_FOUND" "$BSC_LAST_READ_CONFIG" || fail "relative Binary configuration should failed if it does not exists"

  # Creates the binary file.
  local _completeBinPath="$_configDirPath/$_configPathComplex/$_configBinFileName"
  touch "$_completeBinPath" || fail "Create Binary file"
  chmod +x "$_completeBinPath" || fail "Change Binary file permission"

  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_BIN"
  assertEquals "$BSC_CONFIG_NOT_FOUND" "$BSC_LAST_READ_CONFIG" || fail "relative Binary configuration should failed if PATH is not configured properly"

  PATH=$PATH:"$_configDirPath/$_configPathComplex" checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_BIN"
  assertEquals "$_configBinFileName" "$BSC_LAST_READ_CONFIG" || fail "relative Binary configuration should be OK when PATH is properly configured"

  _createSimpleConfigurationFile "$BSC_CONFIG_FILE" "$_configKey" "$_completeBinPath"
  checkAndSetConfig "$_configKey" "$BSC_CONFIG_TYPE_BIN"
  assertEquals "$_completeBinPath" "$BSC_LAST_READ_CONFIG" || fail "absolute Binary configuration should be OK when it exists"

  # Very important to switch off this mode to keep on testing others features.
  export BSC_MODE_CHECK_CONFIG=0

  exitingTests "config"
}

testConfigurationFileAdvancedFeature() {
  local _configKey="my.config.key"
  local _configKey2="another.config.key"
  local _configValue="my Value"
  local _configValue2="my second Value"
  local _configFileAdvanced="$BSC_TMP_DIR/localConfigurationFileAdvanced.conf"

  enteringTests "configAdvanced"

  # Compatibility safe-guard.
  if [ "${_BSC_COMPAT_ASSOCIATIVE_ARRAY:-0}" -eq 0 ]; then
    # This feature can only work with associative array.
    warning "Your GNU/Bash version '$BASH_VERSION' does not support associative array. Disabled advanced configuration file feature tests."
  else
    export BSC_CONFIG_FILE="$_configFileAdvanced"

    # Tests config keys listing, without any configuration file at all.
    [ -z "$( listConfigKeys )" ] || fail "Configuration key listing, with no pattern and no configuration file at all, should work"

    # Tests config <key, value> listing, without any configuration file at all.
    loadConfigKeyValueList
    [ -z "${!BSC_LAST_READ_CONFIG_KEY_VALUE_LIST[*]}" ] || fail "Configuration <key, value> listing, with no pattern and no configuration file at all, should work"

    # Create a configuration file (including comment, and extra spaces which should be ignored).
    info "Creating the temporary advanced configuration file '$_configFileAdvanced', and configuration key should then be found."
cat > "$BSC_CONFIG_FILE" <<EOF
#$_configKey="WRONG$_configValue"
    $_configKey="$_configValue"
#$_configKey="WRONG$_configValue"
$_configKey2="$_configValue2"
#mustNotBeInList$_configKey="$_configValue"
EOF

    # Tests config key listing.
    declare -a expectedCompleteConfigKeyList=( "$_configKey" "$_configKey2" ) # N.B.: order is the one appearing in the file
    IFS= read -r -a readCompleteConfigKeyList <<< "$( listConfigKeys )"
    assertEquals "${expectedCompleteConfigKeyList[*]}" "${readCompleteConfigKeyList[*]}" || fail "Configuration key listing, without pattern, should work"

    assertEquals "$_configKey2" "$( listConfigKeys ".*${_configKey2:1:3}.*" )" || fail "Configuration key listing, with existing pattern, should work"

    [ -z "$( listConfigKeys "doesNotExist" )" ] || fail "Configuration key listing, with NOT existing pattern, should not return anything"

    # Tests config <key, value> listing, with user configuration file, without pattern.
    declare -A expectedCompleteConfigKeyValueList=( ["$_configKey"]="$_configValue" ["$_configKey2"]="$_configValue2" ) # N.B.: order is the one appearing in the file
    loadConfigKeyValueList
    #  Checks count of elements.
    assertEquals "${#expectedCompleteConfigKeyValueList[@]}" "${#BSC_LAST_READ_CONFIG_KEY_VALUE_LIST[@]}" || fail "Configuration <key, value> listing, with no pattern, count of element is not GOOD"

    #  Checks each existing element.
    for configKey in "${!expectedCompleteConfigKeyValueList[@]}"; do
      assertEquals "${expectedCompleteConfigKeyValueList[$configKey]}" "${BSC_LAST_READ_CONFIG_KEY_VALUE_LIST[$configKey]:-}" || fail "Configuration <key, value> listing, with no pattern, element not existing or different value"
    done

    # Tests config <key, value> listing, with user configuration file, with search pattern.
    declare -A expectedCompleteConfigKeyValueList=( ["$_configKey2"]="$_configValue2" ) # N.B.: order is the one appearing in the file
    loadConfigKeyValueList ".*${_configKey2:1:3}.*"
    #  Checks count of elements.
    assertEquals "${#expectedCompleteConfigKeyValueList[@]}" "${#BSC_LAST_READ_CONFIG_KEY_VALUE_LIST[@]}" || fail "Configuration <key, value> listing, with search pattern, count of element is not GOOD"

    #  Checks each existing element.
    for configKey in "${!expectedCompleteConfigKeyValueList[@]}"; do
      assertEquals "${expectedCompleteConfigKeyValueList[$configKey]}" "${BSC_LAST_READ_CONFIG_KEY_VALUE_LIST[$configKey]:-}" || fail "Configuration <key, value> listing, with search pattern, element not existing or different value"
    done

    # Tests config <key, value> listing, with user configuration file, with search and remove pattern.
    declare -A expectedCompleteConfigKeyValueList=( ["${_configKey2:8}"]="$_configValue2" ) # N.B.: order is the one appearing in the file
    loadConfigKeyValueList ".*${_configKey2:1:3}.*" "${_configKey2:0:8}"
    #  Checks count of elements.
    assertEquals "${#expectedCompleteConfigKeyValueList[@]}" "${#BSC_LAST_READ_CONFIG_KEY_VALUE_LIST[@]}" || fail "Configuration <key, value> listing, with search and key remove patterns, count of element is not GOOD"

    #  Checks each existing element.
    for configKey in "${!expectedCompleteConfigKeyValueList[@]}"; do
      assertEquals "${expectedCompleteConfigKeyValueList[$configKey]}" "${BSC_LAST_READ_CONFIG_KEY_VALUE_LIST[$configKey]:-}" || fail "Configuration <key, value> listing, with search and key remove patterns, element not existing or different value"
    done
  fi

  # Very important to switch off this mode to keep on testing others features.
  export BSC_MODE_CHECK_CONFIG=0

  exitingTests "configAdvanced"
}

# Lines feature Tests.
testLinesFeature() {
  local _fileToCheck _fromLine _toLine _result
  _fileToCheck="$0"
  _fromLine=4
  _toLine=8

  enteringTests "lines"

  # TODO: creates a dedicated test file, and ensures the result ... + test all limit cases
  _result=$( getLastLinesFromN "$_fileToCheck" "$_fromLine" ) || fail "Getting lines of file '$_fileToCheck', from line '$_fromLine'"

  _result=$( getLinesFromNToP "$_fileToCheck" "$_fromLine" "$_toLine" ) || fail "Getting lines of file '$_fileToCheck', from line '$_fromLine', to line '$_toLine' (extract lines)"
  [ "$( echo "$_result" |wc -l )" -ne $((_toLine - _fromLine + 1)) ] && fail "Getting lines of file '$_fileToCheck', from line '$_fromLine', to line '$_toLine' (count of lines)"

  exitingTests "lines"
}

# PID file feature Tests, without the Daemon layer which is tested elsewhere.
testPidFileFeature() {
  local _pidFile
  enteringTests "pidFiles"

  _pidFile="$BSC_PID_DIR/testPidFileFeature.pid"

  # Limit tests, on not existing PID file.
  rm -f "$_pidFile"
  deletePIDFile "$_pidFile" || fail "deletePIDFile on not existing file, must NOT fail"

  getPIDFromFile "$_pidFile" && fail "getPIDFromFile with a not existing file, must produce an ERROR"

  getProcessNameFromFile "$_pidFile" && fail "getProcessNameFromFile with a not existing file, must produce an ERROR"

  isRunningProcess "$_pidFile" && fail "isRunningProcess with a not existing file, must produce an ERROR"

  checkAllProcessFromPIDFiles "$BSC_PID_DIR" || fail "checkAllProcessFromPIDFiles on empty PID directory, must NOT fail"

  # Normal situation.
  writePIDFile "$_pidFile" "$0" || fail "Create properly a PID file for this process"
  writePIDFile "$_pidFile" "$0" && fail "Trying to write in the existing PID file, must produce an ERROR"

  isRunningProcess "$_pidFile" || fail "Check if system consider this process as still running"

  checkAllProcessFromPIDFiles "$BSC_PID_DIR" || fail "checkAllProcessFromPIDFiles on PID directory containing PID file(s), must NOT fail"

  deletePIDFile "$_pidFile" || fail "Delete the PID file"

  exitingTests "pidFiles"
}

# Tests Daemon feature.
_doTestDaemonFeature() {
  local _daemonTestName="$1" _daemonTestSample="$2"
  local _daemonParentDir _daemonPIDDir _daemonName _daemonCompletePath

  enteringTests "daemon"

  _daemonParentDir="$BSC_TMP_DIR/$_daemonTestName"
  _daemonPIDDir="$_daemonParentDir/_pids"
  _daemonName="$_daemonTestName.sh"
  _daemonCompletePath="$_daemonParentDir/$_daemonName"

  # Environment creation.
  updateStructure "$_daemonParentDir" || fail "Update structure"
  rm -f "$_daemonCompletePath" || fail "Clean Daemon file"
  cp "$_daemonTestSample" "$_daemonCompletePath" || fail "Create daemon file"
  chmod +x "$_daemonCompletePath" || fail "Change daemon file permission"

  BSC_TMP_DIR="$_daemonParentDir" BSC_PID_DIR="$_daemonPIDDir" \
      "$_daemonCompletePath" -notExistingOption >>"$BSC_LOG_FILE" 2>&1 && fail "Using a not existing option on Daemon CLI sould failed"

  BSC_TMP_DIR="$_daemonParentDir" BSC_PID_DIR="$_daemonPIDDir" \
      "$_daemonCompletePath" -h >>"$BSC_LOG_FILE" && fail "Requesting Daemon usage"

  BSC_TMP_DIR="$_daemonParentDir" BSC_PID_DIR="$_daemonPIDDir" \
      "$_daemonCompletePath" -T || fail "Checking the status of the Daemon ... expected result: NOT running"

  BSC_TMP_DIR="$_daemonParentDir" BSC_PID_DIR="$_daemonPIDDir" \
      "$_daemonCompletePath" -K || fail "Requesting to stop the Daemon ... (warning can occur because of the process killing) expected result: NOT running"

  BSC_TMP_DIR="$_daemonParentDir" BSC_PID_DIR="$_daemonPIDDir" \
      "$_daemonCompletePath" -S || fail "Starting the Daemon"
  sleep 2

  BSC_TMP_DIR="$_daemonParentDir" BSC_PID_DIR="$_daemonPIDDir" \
      "$_daemonCompletePath" -T || fail "Checking the status of the Daemon ... expected result: running"

  BSC_TMP_DIR="$_daemonParentDir" BSC_PID_DIR="$_daemonPIDDir" \
      "$_daemonCompletePath" -K || fail "Requesting to stop the Daemon ... expected result: NOT running"

  BSC_TMP_DIR="$_daemonParentDir" BSC_PID_DIR="$_daemonPIDDir" \
      "$_daemonCompletePath" -T || fail "Checking the status of the Daemon ... expected result: NOT running"

  exitingTests "daemon"
}

testDaemonFeature1() {
  _doTestDaemonFeature "testDaemonFeatureLaunchingTPTool" "$daemonSample1"
}

testDaemonFeature2() {
  _doTestDaemonFeature "testDaemonFeatureLaunchingItsOwnInstructions" "$daemonSample2"
}

# Tests pattern matching feature.
testPatternMatchingFeature() {
    local _pattern1="[My ]*f?irst[ pattern]*" _pattern2="[ \t][0-9][0-9]*[ \t]" _pattern3="^NoThing[ \t]*Void$"
    declare -a patterns=( "$_pattern1" "$_pattern2" "$_pattern3")

    enteringTests "patternMatching"

    # matchesOneOf
    matchesOneOf "ShouldNotMatchAnything" "${patterns[@]}" && fail "Pattern matching should NOT have matched"
    matchesOneOf "irst" "${patterns[@]}" || fail "Pattern matching should have matched with pattern 1"
    matchesOneOf "Yeah this 56 tests should be OK" "${patterns[@]}" || fail "Pattern matching should have matched with pattern 2"
    matchesOneOf "NoThingVoid" "${patterns[@]}" || fail "Pattern matching should have matched with pattern 3"
    matchesOneOf "Thing" "${patterns[@]}" && fail "Pattern matching should NOT have matched"

    # isNumber && isCompoundedNumber
    isNumber "0" || fail "isNumber, bad answer on '0'"
    isNumber "054" || fail "isNumber, bad answer on '054'"
    isNumber "999999" || fail "isNumber, bad answer on '999999'"
    isNumber "5-9" && fail "isNumber, bad answer on '5-9'"

    isCompoundedNumber "0" || fail "isCompoundedNumber, bad answer on '0'"
    isCompoundedNumber "054" || fail "isCompoundedNumber, bad answer '054'"
    isCompoundedNumber "999999" || fail "isCompoundedNumber, bad answer on '999999'"
    isCompoundedNumber "5-9" || fail "isCompoundedNumber, bad answer on '5-9'"
    isCompoundedNumber "30-098" || fail "isCompoundedNumber, bad answer on '30-098'"

    # removeAllSpecifiedPartsFromString
    local _string="NothingToChange" _result
    _result=$( removeAllSpecifiedPartsFromString "$_string" "$_pattern1|$_pattern2|$_pattern3" )
    assertEquals "$_result" "$_string" || fail "removeAllSpecifiedPartsFromString failure on '$_string'"

    _string="MyFirstString"
    _result=$( removeAllSpecifiedPartsFromString "$_string" "$_pattern1|$_pattern2|$_pattern3" )
    assertEquals "$_result" "MyFString" || fail "removeAllSpecifiedPartsFromString failure on '$_string' with case sensitive"
    _result=$( removeAllSpecifiedPartsFromString "$_string" "$_pattern1|$_pattern2|$_pattern3" "1")
    assertEquals "$_result" "String" || fail "removeAllSpecifiedPartsFromString failure on '$_string' with case insensitive"

    _string="MyFirstString 999999 "
    _result=$( removeAllSpecifiedPartsFromString "$_string" "$_pattern1|$_pattern2|$_pattern3" "1" )
    assertEquals "$_result" "String" || fail "removeAllSpecifiedPartsFromString failure on '$_string'"

    # extractNumberSequence
    assertEquals "$( extractNumberSequence "LotsOfUselessThings_2_AndSomeMore.myExtension")" "2" || fail "extractNumberSequence failure on '2'"
    assertEquals "$( extractNumberSequence "LotsOfUselessThings.3.AndSomeMore.myExtension")" "3" || fail "extractNumberSequence failure on '3'"
    assertEquals "$( extractNumberSequence "LotsOfUselessThings.E5.AndSomeMore.myExtension")" "5" || fail "extractNumberSequence failure on '5'"
    assertEquals "$( extractNumberSequence "LotsOfUselessThings.7-9.AndSomeMore.myExtension")" "7-9" || fail "extractNumberSequence failure on '7-9'"
    assertEquals "$( extractNumberSequence "LotsOfUselessThings.11 à 13.AndSomeMore.myExtension")" "11-13" || fail "extractNumberSequence failure on '11-13'"
    assertEquals "$( extractNumberSequence "LotsOfUselessThings.17 & 19.AndSomeMore.myExtension")" "17-19" || fail "extractNumberSequence failure on '17-19'"
    assertEquals "$( extractNumberSequence "LotsOfUselessThings29-29AndSomeMore.myExtension")" "29-29" || fail "extractNumberSequence failure on '29-29'"

    exitingTests "patternMatching"
}

# Tests compatibility mode feature.
testCompatibilityMode() {
  enteringTests "compatibilityMode"

    if [ "${_BSC_COMPAT_ASSOCIATIVE_ARRAY:-0}" -eq 0 ]; then
      # This feature can only work with associative array.
      warning "Your GNU/Bash version '$BASH_VERSION' does not support associative array. Disabled compatibility feature tests."
    else
      BSC_FORCE_COMPAT_MODE=0 loadConfigKeyValueList
      assertEquals "$( grep -c "does not support associative array" <<<"$BSC_LAST_READ_CONFIG_KEY_VALUE_LIST" )" "1"  || fail "loadConfigKeyValueList failure with compatibility mode forced to off"
    fi

    if [ "${_BSC_COMPAT_DATE_PRINTF:-0}" -eq 0 ]; then
      # This feature can only work with associative array.
      warning "Your GNU/Bash version '$BASH_VERSION' does not support printf date feature. Disabled compatibility feature tests."
    else
      local _result1 _result2
      _result1=$(BSC_FORCE_COMPAT_MODE=0 getFormattedDatetime '%Y-%m-%d %H' )
      _result2=$(BSC_FORCE_COMPAT_MODE=1 getFormattedDatetime '%Y-%m-%d %H' )
      assertEquals "$_result1" "$_result2" || fail "getFormattedDatetime failure with/out compatibility mode forced"
    fi

    exitingTests "compatibilityMode"
}

# Tests Third Party Tools Management feature.
_doTestSpecificToolManagementFeature() {
  local _featureFunc="$1" _envVarName="$2" _configKey="$3"
  local _badBinPathToCheck="$4|" _goodBinPathToCheck="$5"
  local _configFileSimple="$BSC_TMP_DIR/specificToolConfigurationFile_$_featureFunc.conf"

  enteringTests "specificToolCheck:$_featureFunc"

  export BSC_MODE_CHECK_CONFIG=1

  ## Tests with environment variable definition.
  local _badBinPathNumber=1
  while IFS= read -r -d '|' binPathToCheck; do
    export "$_envVarName"="$binPathToCheck"
    $_featureFunc && fail "$_featureFunc should failed with a badly configured environment variable #$_badBinPathNumber"
    _badBinPathNumber=$((_badBinPathNumber+1))
  done <<<"$_badBinPathToCheck"

  export "$_envVarName"="$_goodBinPathToCheck"
  $_featureFunc || fail "$_featureFunc should success with a properly configured environment variable"

  ## Tests with configuration file definition.
  # Without any configuration, it should failed.
  unset "$_envVarName"
  $_featureFunc && fail "$_featureFunc should failed without any configuration"


  _badBinPathNumber=1
  while IFS= read -r -d '|' binPathToCheck; do
    info "Creating the temporary simple configuration file '$_configFileSimple', and configuration key corresponding to specific tool, with bad path #$_badBinPathNumber"
    _createSimpleConfigurationFile "$_configFileSimple" "$_configKey" "$binPathToCheck"
    $_featureFunc && fail "$_featureFunc should failed with a badly set configuration file #$_badBinPathNumber"
    unset "$_envVarName"

    _badBinPathNumber=$((_badBinPathNumber+1))
  done <<<"$_badBinPathToCheck"

  # Create a simple configuration file, with a good path.
  info "Creating the temporary simple configuration file '$_configFileSimple', and configuration key corresponding to specific tool, with good path."
  _createSimpleConfigurationFile "$_configFileSimple" "$_configKey" "$_goodBinPathToCheck"
  $_featureFunc || fail "$_featureFunc should success with a properly configured path"
  unset "$_envVarName"

  # Very important to switch off this mode to keep on testing others features.
  export BSC_MODE_CHECK_CONFIG=0

  exitingTests "specificToolCheck:$_featureFunc"
}

testJavaToolManagementFeature() {
  _doTestSpecificToolManagementFeature "manageJavaHome" "JAVA_HOME" "environment.java.home" \
            "/does/not/exists|/usr/bin/java|/usr/bin" "/usr"
}

testAntToolManagementFeature() {
  _doTestSpecificToolManagementFeature "manageAntHome" "ANT_HOME" "environment.ant.home" \
            "/does/not/exists|/usr/bin/ant|/usr/bin" "/usr"
}

testMavenToolManagementFeature() {
  _doTestSpecificToolManagementFeature "manageMavenHome" "M2_HOME" "environment.maven.home" \
            "/does/not/exists|/usr/bin/mvn|/usr/bin" "/usr"
}

# Tests URL contents feature.
testURLContentsFeature() {
  local _url="https://gitlab.com/bertrand-benoit/scripts-common"
  local _output="$BSC_TMP_DIR/testURLContents.html"

  enteringTests "getURLContents"
  getURLContents "$_url" "$_output" || fail "getURLContents should not failed"

  getURLContents "$_url" "$_output"_2 "Mozilla/5.0" || fail "getURLContents should not failed while specifying User Agent"

  exitingTests "getURLContents"
}

# Triggers shUnit 2.
# shellcheck disable=1090
. "$shunitUtilities" || failureOccurs=1

echo "Logs file: $BSC_LOG_FILE"
exit ${failureOccurs:-0}
