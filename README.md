# scripts-common tester version 1.0

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/30f3d380d4c846689aaccfbc87b1a883)](https://www.codacy.com/manual/gitlabRepositories/scripts-common-tests_2?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=bertrand-benoit/scripts-common-tests&amp;utm_campaign=Badge_Grade)

[scripts-common](https://gitlab.com/bertrand-benoit/scripts-common) is a free common utilities/tool-box for GNU/Bash scripts, you can use for your own scripts.

This project aims to test it.

It was initially embedded in original repository.

It was then removed to avoid to be checkout in all client projects.

And eventually, it evolves to use [shUnit2](https://github.com/kward/shunit2).

## Installation
The easiest way to clone this repository, is using the `--recursive` option to initialize automatically shUnit2 submodule in the same time:
```
git clone --recursive git@gitlab.com:bertrand-benoit/scripts-common-tests.git
```

For any reason, if you prefer initializing submodule later, you can clone as usually, and then perform these instructions later:
```
git submodule init
git submodule update
```

### Tests coverage
Under Gitlab, [scripts-common CI/CD](https://gitlab.com/bertrand-benoit/scripts-common/-/blob/master/.gitlab-ci.yml) is configured to use [Bashcov](https://github.com/infertux/bashcov) to report tests coverage.

You can easily use it locally.

## Usage
This version uses [shUnit2](https://github.com/kward/shunit2), so you cannot use CLI options; thus you must define the **SCRIPTS_COMMON_PATH** environment variable with the utilities version you want to test.
```
export SCRIPTS_COMMON_PATH="<path to define>/scripts-common/utilities.sh"
./tests.sh
```

### Launch specific tests
If you want to launch specific tests, for instance when [Contributing](#Contributing) to project, you can append the CLI with `--` option and then add any wanted tests function.
For instance:
```
./tests.sh -- testVersionFeature testConfigurationFileAdvancedFeature
```


## Contributing
Don't hesitate to [contribute](https://opensource.guide/how-to-contribute/) or to contact me if you want to improve the project.
You can [report issues or request features](https://gitlab.com/bertrand-benoit/scripts-common-tests/issues) and propose [merge requests](https://gitlab.com/bertrand-benoit/scripts-common-tests/merge_requests).

## Versioning
The versioning scheme we use is [SemVer](http://semver.org/).

## Authors
[Bertrand BENOIT](mailto:contact@bertrand-benoit.net)

## License
This project is under the GPLv3 License - see the [LICENSE](LICENSE) file for details
